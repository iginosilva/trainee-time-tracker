# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

# Users
john = User.create({ name: 'John Wick', email: 'john@email.com', password: '123456', password_confirmation: '123456' })
jack = User.create({ name: 'Jack Reacher', email: 'jack@email.com', password: '123456', password_confirmation: '123456' })

# Times
TimeTrackingLog.create({ time_entry: Time.current.change(hour: 8, min: 0), user_id: john.id })
TimeTrackingLog.create({ time_entry: Time.current.change(hour: 12, min: 0), user_id: john.id })
TimeTrackingLog.create({ time_entry: Time.current.change(hour: 14, min: 0), user_id: john.id })
TimeTrackingLog.create({ time_entry: Time.current.change(hour: 18, min: 0), user_id: john.id })


TimeTrackingLog.create({ time_entry: Time.current.change(hour: 8, min: 30), user_id: jack.id })
TimeTrackingLog.create({ time_entry: Time.current.change(hour: 12, min: 30), user_id: jack.id })
TimeTrackingLog.create({ time_entry: Time.current.change(hour: 14, min: 30), user_id: jack.id })
TimeTrackingLog.create({ time_entry: Time.current.change(hour: 18, min: 45), user_id: jack.id })