class CreateTimeTrackingLogs < ActiveRecord::Migration[5.2]
  def change
    create_table :time_tracking_logs do |t|
      t.datetime :time_entry
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end
