require "application_system_test_case"

class TimeTrackingLogsTest < ApplicationSystemTestCase
  setup do
    @time_tracking_log = time_tracking_logs(:one)
  end

  test "visiting the index" do
    visit time_tracking_logs_url
    assert_selector "h1", text: "Time Tracking Logs"
  end

  test "creating a Time tracking log" do
    visit time_tracking_logs_url
    click_on "New Time Tracking Log"

    fill_in "Time entry", with: @time_tracking_log.time_entry
    fill_in "User", with: @time_tracking_log.user_id
    click_on "Create Time tracking log"

    assert_text "Time tracking log was successfully created"
    click_on "Back"
  end

  test "updating a Time tracking log" do
    visit time_tracking_logs_url
    click_on "Edit", match: :first

    fill_in "Time entry", with: @time_tracking_log.time_entry
    fill_in "User", with: @time_tracking_log.user_id
    click_on "Update Time tracking log"

    assert_text "Time tracking log was successfully updated"
    click_on "Back"
  end

  test "destroying a Time tracking log" do
    visit time_tracking_logs_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Time tracking log was successfully destroyed"
  end
end
