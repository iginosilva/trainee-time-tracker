require 'test_helper'

class TimeTrackingLogsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @time_tracking_log = time_tracking_logs(:one)
  end

  test "should get index" do
    get time_tracking_logs_url
    assert_response :success
  end

  test "should get new" do
    get new_time_tracking_log_url
    assert_response :success
  end

  test "should create time_tracking_log" do
    assert_difference('TimeTrackingLog.count') do
      post time_tracking_logs_url, params: { time_tracking_log: { time_entry: @time_tracking_log.time_entry, user_id: @time_tracking_log.user_id } }
    end

    assert_redirected_to time_tracking_log_url(TimeTrackingLog.last)
  end

  test "should show time_tracking_log" do
    get time_tracking_log_url(@time_tracking_log)
    assert_response :success
  end

  test "should get edit" do
    get edit_time_tracking_log_url(@time_tracking_log)
    assert_response :success
  end

  test "should update time_tracking_log" do
    patch time_tracking_log_url(@time_tracking_log), params: { time_tracking_log: { time_entry: @time_tracking_log.time_entry, user_id: @time_tracking_log.user_id } }
    assert_redirected_to time_tracking_log_url(@time_tracking_log)
  end

  test "should destroy time_tracking_log" do
    assert_difference('TimeTrackingLog.count', -1) do
      delete time_tracking_log_url(@time_tracking_log)
    end

    assert_redirected_to time_tracking_logs_url
  end
end
