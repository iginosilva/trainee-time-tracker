Rails.application.routes.draw do
  resources :time_tracking_logs
  root to: 'time_tracking_logs#index'
  resources :users, only: [:new, :create]

  get '/login'     => 'sessions#new'
	post '/login'    => 'sessions#create'
	delete '/logout' => 'sessions#destroy'
end
