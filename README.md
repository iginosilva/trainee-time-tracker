# Trainee Time Tracker

Um simples apontador de horas feito em Ruby on Rails, Bootstrap 4 e Mysql. Inclui suporte a autenticação de usuários e histórico de horas registradas, restritas a tela do usuário à qual pertence.

## Requisitos

- [Ruby 2.6.5](https://www.ruby-lang.org/pt/downloads/)
- [Rails 5.2.4](https://guides.rubyonrails.org/getting_started.html)
- [Mysql](https://www.mysql.com/)

## Execução

- Baixe o arquivo dump do mysql através do link:
  
    [trainee-time-tracker-dump.sql - Google Drive](https://drive.google.com/open?id=1gWTt0kL8MIwJIwHts435N1ViIsEPnl7v)

- Credenciais do banco: Usuário: trainee / Senha: trainee

- Certifique-se que em seu Mysql local exista um banco chamado "trainee-time-tracker_development", caso contrário, crie;

- Importe o banco de dados com o comando:
  
    `mysql -u trainee -p trainee-time-tracker_development < trainee-time-tracker-dump.sql`

- Caso não queira importar o banco de dados através do arquivo de dump, você poderá criar os registros atavés do seed:

    `rake db:seed`

- Aqui está um vídeo mostrando o funcionamento da aplicação (criação de contas, login, apontamento de horas):

    [TraineeTimeTracker.webm - Google Drive](https://drive.google.com/file/d/1s1bSVxshe8wJ_slNZR07mnyiwa8oaUU6/view)


- Inicie o servidor do rails e aplicação estará pronta:

    `rails server`