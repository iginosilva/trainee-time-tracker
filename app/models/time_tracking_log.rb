class TimeTrackingLog < ApplicationRecord
  belongs_to :user

  def self.today
    initial_time = Time.current.beginning_of_day
    end_time = Time.current.end_of_day
    where(time_entry: initial_time..end_time).order(time_entry: :asc)
  end

  def self.last_seven_days
    initial_time = Time.current.beginning_of_day - 7.days
    end_time = Time.current.end_of_day
    where(time_entry: initial_time..end_time).order(time_entry: :asc)
  end
end
