class TimeTrackingLogsController < ApplicationController
  before_action :set_time_tracking_log, only: [:show, :edit, :update, :destroy]
  before_action :authorize

  def index
    @today_time_log = sum_times(@current_user.try(:time_tracking_logs).try(:today))
    @last_seven_days_time_log = sum_times(@current_user.try(:time_tracking_logs).try(:last_seven_days))
  end

  def show
  end

  def new
    @time_tracking_log = TimeTrackingLog.new
  end

  def edit
  end

  def create
    @time_tracking_log = TimeTrackingLog.new(time_tracking_log_params)
    @time_tracking_log.time_entry = Time.current
    @time_tracking_log.user_id = @current_user.id

    respond_to do |format|
      if @time_tracking_log.save
        format.html { redirect_to time_tracking_logs_path, notice: 'Registro criado com sucesso.' }
        format.json { render :show, status: :created, location: @time_tracking_log }
      else
        format.html { render :new }
        format.json { render json: @time_tracking_log.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @time_tracking_log.update(time_tracking_log_params)
        format.html { redirect_to @time_tracking_log, notice: 'Registro atualizado com sucesso.' }
        format.json { render :show, status: :ok, location: @time_tracking_log }
      else
        format.html { render :edit }
        format.json { render json: @time_tracking_log.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @time_tracking_log.destroy
    respond_to do |format|
      format.html { redirect_to time_tracking_logs_url, notice: 'Registro deletado com sucesso.' }
      format.json { head :no_content }
    end
  end

  private
    def sum_times(time_entries)
      total_time_in_seconds = 0
      time_entries.each_slice(2) do |double_times|
        if double_times.size === 2
          times = double_times.sort.reverse
          total_seconds = times.first.time_entry - times.last.time_entry
          total_time_in_seconds += total_seconds
        end
      end

      { times: time_entries, total_sum_times: Time.at(total_time_in_seconds).utc.strftime("%H:%M:%S") }
    rescue
      nil
    end

    def set_time_tracking_log
      @time_tracking_log = TimeTrackingLog.find(params[:id])
    end

    def time_tracking_log_params
      params.require(:time_tracking_log).permit(:time_entry, :user_id)
    end
end
