class UsersController < ApplicationController
  def new
    @user = User.new
  end

  def create
    @user = User.new(user_params)
    @user.email.downcase!
    
    if @user.save
      flash[:notice] = "Conta criada com sucesso!"
      redirect_to root_path
    else
      flash.now.alert = "A conta não pôde ser criada. Por favor, insira um e-mail e senha válidos."
      render :new
    end
  end

  private

  def user_params
    params.require(:user).permit(:name, :email, :password, :password_confirmation)
  end
end
