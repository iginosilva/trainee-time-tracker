json.extract! time_tracking_log, :id, :time_entry, :user_id, :created_at, :updated_at
json.url time_tracking_log_url(time_tracking_log, format: :json)
