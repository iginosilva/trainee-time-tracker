module ApplicationHelper
  def br_date(datetime)
    datetime.strftime("%d/%m/%Y")
  end

  def br_time(datetime)
    datetime.strftime("%I:%M%p")
  end
end
